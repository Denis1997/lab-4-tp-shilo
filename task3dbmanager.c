#include <stdio.h>
#include <sqlite3.h>
#include <string.h>
#include <stdlib.h>
#include "Person.h"

#define TRUE 1
#define FALSE 0

typedef int BOOL;

void showMenu();
int getMenuRequest(int l, int r);
static int displayInfo(void *NotUsed, int argc, char **argv, char **ColName);
int getCorrectId();

BOOL isCorrectDate(char *);
BOOL isCorrectDateFormat(char *);
BOOL getCorrectDate(char *);
BOOL getCorrectPerson(Person *);

BOOL getInfoAboutEverybody(sqlite3 *db);
BOOL findPersonByID(sqlite3 *db);
BOOL findPersonsWithSecondname(sqlite3 *db);
BOOL findPersonWithCountry(sqlite3 *db);
BOOL insertNewEmployee(sqlite3 *db);
BOOL insertSeveralNewEmployees(sqlite3 *db);
BOOL deleteEmployeeById(sqlite3 *bd);

BOOL printPhotoOfEmployeeWithId(sqlite3 *db);
char * getImageByteSeq(char * filename, int *size);

int main(int argc, char * argv[])
{
    sqlite3 *db;
    int ans;
    int rc;
    
    if (argc <= 1)
    {
        fprintf(stderr, "No database to open\n");
    }
    rc = sqlite3_open(argv[1], &db);
    
    do {
        showMenu();
        ans = getMenuRequest(0, 8);
        
        switch (ans) {
            case 0:
                break;
            case 1:
                getInfoAboutEverybody(db);
                break;
            case 2:
                insertNewEmployee(db);
                break;
            case 3:
                deleteEmployeeById(db);
                break;
            case 4:
                findPersonByID(db);
                break;
            case 5:
                findPersonsWithSecondname(db);
                break;
            case 6:
                findPersonWithCountry(db);
                break;
            case 7:
                insertSeveralNewEmployees(db);
                break;
            case 8:
                printPhotoOfEmployeeWithId(db);
                break;
            default:
                break;
        }
    }while(ans != 0);
    
    sqlite3_close(db);
    return 0;
}

BOOL insertSeveralNewEmployees(sqlite3 *db)
{
    char *zErrMsg;
    int ans;
    int n;
    sqlite3_stmt *res;
    
    printf("Enter number of new employees:");
    scanf("%d", &n);
    for (int i = 0; i < n; i++)
    {
        printf("Enter information about %d employee.\n", i + 1);
        insertNewEmployee(db);
    }
    return TRUE;
}

BOOL insertNewEmployee(sqlite3 *db)
{
    char *sql = "INSERT INTO Employees(Firstname, Secondname, Thirdname, Birthdate, Photo, City, Country"\
    ", FullAddress, Department, Post, HireDate) VALUES(@fname, @sname, @tname, @bdate,"\
            " @photo, @bplace, @country, @faddress, @dep, @post, @hdate);";
    sqlite3_stmt *res;
    Person p;
    int idx;
    size_t faddressSize = 100;
    
    int ans = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    
    if (ans != SQLITE_OK)
    {
        printf("Operation failed. %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(res);
        return FALSE;
    }
    
    idx = sqlite3_bind_parameter_index(res, "@fname");
    printf("Enter firstname: ");
    scanf("%s", p.name);
    sqlite3_bind_text(res, idx, p.name, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@sname");
    printf("Enter secondname: ");
    scanf("%s", p.secondName);
    sqlite3_bind_text(res, idx, p.secondName, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@tname");
    printf("Enter thirdname: ");
    scanf("%s", p.thirdName);
    sqlite3_bind_text(res, idx, p.thirdName, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@bdate");
    printf("Enter birthdate (YYYY-MM-DD): ");
    scanf("%s", p.birthDate);
    sqlite3_bind_text(res, idx, p.birthDate, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@bplace");
    printf("Enter birthplace: ");
    scanf("%s", p.birthPlace);
    sqlite3_bind_text(res, idx, p.birthPlace, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@country");
    printf("Enter country: ");
    scanf("%s", p.country);
    sqlite3_bind_text(res, idx, p.country, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@faddress");
    printf("Enter full address: ");
    getchar();
    scanf("%[^\n]", p.fullAddress);
    sqlite3_bind_text(res, idx, p.fullAddress, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@dep");
    printf("Enter department: ");
    scanf("%s", p.department);
    sqlite3_bind_text(res, idx, p.department, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@post");
    printf("Enter post: ");
    scanf("%s", p.post);
    sqlite3_bind_text(res, idx, p.post, -1, SQLITE_STATIC);
    
    idx = sqlite3_bind_parameter_index(res, "@hdate");
    printf("Enter hire date: ");
    scanf("%s", p.hireDate);
    sqlite3_bind_text(res, idx, p.hireDate, -1, SQLITE_STATIC);
    

    {
        char *image = NULL;
        int size = 0;
            int j = 0;
        idx = sqlite3_bind_parameter_index(res, "@photo");
        printf("Enter image file name: ");
        getchar();
        scanf("%[^\n]", p.photo);
    
        image = getImageByteSeq(p.photo, &size);
        for (j = 0; j < size; j++)
            sprintf(image, "%c", image[j]);
        sqlite3_bind_blob(res, idx, image, size, SQLITE_STATIC);
        
        free(image);
    }
    
    ans = sqlite3_step(res);
    
    if (ans != SQLITE_DONE)
    {
        printf("Operation failed %s\n", sqlite3_errmsg(db));
        return FALSE;
    }
    
    sqlite3_finalize(res);
    printf("Insert done.\n");
    return TRUE;
}

BOOL findPersonWithCountry(sqlite3 *db)
{
    char *sql = "SELECT * FROM Employees WHERE Country LIKE ?";
    int ans;
    sqlite3_stmt * res;
    int id;
    char country[100];
    
    ans = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    if (ans != SQLITE_OK)
    {
        printf("Operation failed. %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(res);
        return FALSE;
    }
    
    printf("Enter country: ");
    scanf("%s", country);
    strcat(country, "%");
    
    sqlite3_bind_text(res, 1, country, -1, SQLITE_STATIC);
    
    while (sqlite3_step(res) == SQLITE_ROW)
    {
        int i = 0;
        int colNum = sqlite3_column_count(res);
        for (i = 0; i < colNum; i++)
            printf("\t%s: %s\n", sqlite3_column_name(res, i), sqlite3_column_text(res, i));
        printf("\n");
    }
    sqlite3_finalize(res);
    return TRUE;
}

BOOL findPersonsWithSecondname(sqlite3 *db)
{
    char *sql = "SELECT * FROM Employees WHERE Secondname LIKE ?";
    int ans;
    sqlite3_stmt * res;
    int id;
    char secondname[100];
    
    ans = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    if (ans != SQLITE_OK)
    {
        printf("Operation failed. %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(res);
        return FALSE;
    }
    
    printf("Enter family name: ");
    scanf("%s", secondname);
    strcat(secondname, "%");
    
    sqlite3_bind_text(res, 1, secondname, -1, SQLITE_STATIC);
    
    while (sqlite3_step(res) == SQLITE_ROW)
    {
        int i = 0;
        int colNum = sqlite3_column_count(res);
        for (i = 0; i < colNum; i++)
            printf("\t%s: %s\n", sqlite3_column_name(res, i), sqlite3_column_text(res, i));
        printf("\n");
    }
    sqlite3_finalize(res);
    return TRUE;
}

BOOL findPersonByID(sqlite3 *db)
{
    char *sql = "SELECT * FROM Employees WHERE Id = ?;";
    int ans;
    sqlite3_stmt * res;
    int id;
    
    ans = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    if (ans != SQLITE_OK)
    {
        printf("Operation failed. %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(res);
        return FALSE;
    }
    id = getCorrectId();
    
    sqlite3_bind_int(res, 1, id);
    
    while (sqlite3_step(res) == SQLITE_ROW)
    {
        int i = 0;
        int colNum = sqlite3_column_count(res);
        for (i = 0; i < colNum; i++)
            printf("\t%s: %s\n", sqlite3_column_name(res, i), sqlite3_column_text(res, i));
        printf("\n");
    }
    sqlite3_finalize(res);
    return TRUE;
}

int getCorrectId()
{//ToDo make it safely
    int id = 0;
    printf("Enter id: ");
    scanf("%d", &id);
    return id;
}

int getInfoAboutEverybody(sqlite3 *db)
{
    char *sql = "SELECT * FROM Employees;";
    char *zErrMsg = 0;
    int ans = sqlite3_exec(db, sql, displayInfo, 0, &zErrMsg);
    if (ans != SQLITE_OK)
    {
        printf("Operation failed. %s\n", zErrMsg);
        sqlite3_free(zErrMsg);
        return FALSE;
    }
    return TRUE;
}

static int displayInfo(void *NotUsed, int argc, char **argv, char **ColName)
{
    int i;
    for (i = 0; i < argc; i++)
        printf ("%s = %s\n", ColName[i], argv[i] ? argv[i] : "NULL"	);
    printf("\n");
    return 0;
}

void showMenu()
{
    printf("---------MENU--------\n"\
           " 1) Get information about employees;\n"\
           " 2) Add new employee;\n"\
           " 3) Delete an employee;\n"\
           " 4) Find an employee with selected ID;\n"\
           " 5) Get list of employees with selected family name;\n"\
           " 6) Get list of employees from selected country;\n"\
           " 7) Add several new employees;\n"\
           " 8) Print photo of employee with selected ID;\n"\
           " 0) Exit.\n");
}

int getMenuRequest(int l, int r)
{//ToDo Make it safely
    int ans;
    printf("Enter menu point (%d..%d): ", l, r);
    
    do{
        scanf("%d", &ans);
        if (ans < l || ans > r)
        {
            printf("Index out of range. Try again.\n");
            continue;
        }
        break;
    }while  (TRUE);
    return ans;
}

BOOL deleteEmployeeById(sqlite3 *db)
{
    char *sql = "DELETE FROM Employees WHERE Id = ?;";
    int ans;
    sqlite3_stmt * res;
    int id;
    
    ans = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    if (ans != SQLITE_OK)
    {
        printf("Operation failed. %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(res);
        return FALSE;
    }
    id = getCorrectId();
    
    sqlite3_bind_int(res, 1, id);
    
    ans = sqlite3_step(res);
    if (ans != SQLITE_DONE)
    {
        printf("Operation failed. %s\n", sqlite3_errmsg(db));
        sqlite3_finalize(res);
    }
    sqlite3_finalize(res);
    return TRUE;

    return TRUE;
}

BOOL printPhotoOfEmployeeWithId(sqlite3 *db)
{
    char photoFileName[300];
    printf("Enter photo out file name: ");
    getchar();
    scanf("%[^\n]", photoFileName);
    
    FILE *fp = fopen(photoFileName, "wb");
    
    int id;
    if (fp == NULL) {
        fprintf(stderr, "Cannot open image file\n");
        return FALSE;
    }
    char *err_msg = 0;
    int ans;
    char sql[100];
    
    id = getCorrectId();
    sprintf(sql, "SELECT Photo FROM Employees WHERE Id = %d", id);
    
    sqlite3_stmt *res;
    ans = sqlite3_prepare_v2(db, sql, -1, &res, 0);
    
    if (ans != SQLITE_OK ) {
        
        fprintf(stderr, "Failed to prepare statement\n");
        return FALSE;
    }
    
    ans = sqlite3_step(res);
    
    int bytes = 0;
    
    if (ans == SQLITE_ROW) {
        
        bytes = sqlite3_column_bytes(res, 0);
    }
    
    fwrite(sqlite3_column_blob(res, 0), bytes, 1, fp);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fwrite() failed\n");
        return FALSE;
    }
    
    int r = fclose(fp);
    
    if (r == EOF) {
        fprintf(stderr, "Cannot close file handler\n");
    }
    
    sqlite3_finalize(res);
    return TRUE;
}

char * getImageByteSeq(char * filename, int *size)
{
    FILE *fp = fopen(filename, "rb");
    
    if (fp == NULL) {
        
        fprintf(stderr, "Cannot open image file\n");
        
        return NULL;
    }
    
    fseek(fp, 0, SEEK_END);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fseek() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return NULL;
    }
    
    int flen = ftell(fp);
    
    if (flen == -1) {
        
        perror("error occurred");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return NULL;
    }
    
    fseek(fp, 0, SEEK_SET);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fseek() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return NULL;
    }
    
    char * data = (char *)malloc((flen + 1) * sizeof(char));
    
    *size = fread(data, 1, flen, fp);
    
    if (ferror(fp)) {
        
        fprintf(stderr, "fread() failed\n");
        int r = fclose(fp);
        
        if (r == EOF) {
            fprintf(stderr, "Cannot close file handler\n");
        }
        
        return NULL;
    }
    
    int r = fclose(fp);
    
    if (r == EOF) {
        fprintf(stderr, "Cannot close file handler\n");
    }
    
    return data;
}